import React, { useState } from "react";

const MyProfile = () => {
  const [editing, setEditing] = useState(false);
  const [name, setName] = useState("Shubham Paranjape");
  const [email, setEmail] = useState("shubham.paranjape@meg-nxt.com");
  const [empid, setEmpID] = useState("1011");
  const [department, setDepartment] = useState("Development");
  const [birthday, setBirthday] = useState("21/05/1996");

  const handleEditClick = () => {
    setEditing(true);
  };

  const handleSaveClick = () => {
    setEditing(false);
  };

  const handleCancelClick = () => {
    setEditing(false);
  };

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleEmpidChange = (e) => {
    setEmpID(e.target.value);
  };

  const handleDepartmentChange = (e) => {
    setDepartment(e.target.value);
  };

  const handleBirthdayChange = (e) => {
    setBirthday(e.target.value);
  };

  return (
    <div className="relative flex flex-col justify-center min-h-screen overflow-hidden">
      <div className="w-full p-6 m-auto bg-white rounded-md shadow-xl shadow-rose-600/40 ring-2 ring-indigo-600 lg:max-w-xl">
        <h1 className="text-3xl font-semibold text-center text-indigo-700 ">
          My Profile
        </h1>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="name"
          >
            Name
          </label>
          {editing ? (
            <input
              type="text"
              id="name"
              value={name}
              onChange={handleNameChange}
              className="w-full border border-gray-300 p-2 rounded"
            />
          ) : (
            <p>{name}</p>
          )}
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="empid"
          >
            Employee ID
          </label>
          {editing ? (
            <input
              type="text"
              id="empid"
              value={empid}
              onChange={handleEmpidChange}
              className="w-full border border-gray-300 p-2 rounded"
            />
          ) : (
            <p>{empid}</p>
          )}
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="department"
          >
            Department
          </label>
          {editing ? (
            <input
              type="text"
              id="department"
              value={department}
              onChange={handleDepartmentChange}
              className="w-full border border-gray-300 p-2 rounded"
            />
          ) : (
            <p>{department}</p>
          )}
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="birthday"
          >
            Birthday
          </label>
          {editing ? (
            <input
              type="text"
              id="birthday"
              value={birthday}
              onChange={handleBirthdayChange}
              className="w-full border border-gray-300 p-2 rounded"
            />
          ) : (
            <p>{birthday}</p>
          )}
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="email"
          >
            Email
          </label>
          {editing ? (
            <input
              type="email"
              id="email"
              value={email}
              onChange={handleEmailChange}
              className="w-full border border-gray-300 p-2 rounded"
            />
          ) : (
            <p>{email}</p>
          )}
        </div>

        {editing ? (
          <div className="flex">
            <button
              onClick={handleSaveClick}
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2"
            >
              Save
            </button>
            <button
              onClick={handleCancelClick}
              className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded"
            >
              Cancel
            </button>
          </div>
        ) : (
          <button
            onClick={handleEditClick}
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          >
            Edit
          </button>
        )}
      </div>
    </div>
  );
};

export default MyProfile;
