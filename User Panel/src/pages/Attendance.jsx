import React, { useState } from "react";

const Attendance = () => {
  const [isCheckout, setIsCheckOut] = useState(true);
  const [isCheck, setIsCheck] = useState(false);

  return (
    <div className="relative flex flex-col justify-center min-h-screen overflow-hidden ">
      <div className="w-full p-6 m-auto bg-white rounded-md shadow-xl shadow-rose-600/40 ring-2 ring-indigo-600 lg:max-w-xl">
        <h1 className="text-3xl font-semibold text-center text-indigo-700 ">
          Attendance
        </h1>
        <form className="mt-6">
          <div className="mb-2">
            <label>
              <span className="text-gray-700">Employee Name</span>
              <input
                name="name"
                type="text"
                className="
            block
            w-full
            mt-2 px-16 py-2
            border-gray-300
            rounded-md
            shadow-sm
            focus:border-indigo-300
            focus:ring
            focus:ring-indigo-200
            focus:ring-opacity-50
          "
                placeholder="Shubham Paranjape"
                required
              />
            </label>
          </div>
          <div className="mb-2 flex items-stretch">
            <label>
              <span className="text-gray-700">Employee ID</span>
              <input
                type="text"
                name="EmpID"
                className="

            w-64
            block px-16 py-2 mt-2
            border-gray-300
            rounded-md
            shadow-sm
            focus:border-indigo-300
            focus:ring
            focus:ring-indigo-200
            focus:ring-opacity-50
          "
                placeholder="1011"
              />
            </label>
            <label>
              <span className="text-gray-700">Date</span>
              <input
                name="date"
                type="date"
                className="
            block
            w-64
            mt-2 px-16 py-2
            border-gray-300
            rounded-md
            shadow-sm
            focus:border-indigo-300
            focus:ring
            focus:ring-indigo-200
            focus:ring-opacity-50
          "
                required
              />
            </label>
          </div>

          <div class="mt-6 mb-6 flex justify-center ... flex space-x-12 ...;">
            <button
              type="submit"
              disabled={isCheckout}
              onClick={(e) => {
                console.log("isCheck", isCheckout);
                e.preventDefault();
                setIsCheckOut(!isCheckout);
              }}
              className={`
            h-10
            px-5
            text-indigo-100
             ${isCheckout ? "bg-slate-500" : "bg-indigo-700"}
            rounded-lg
            transition-colors
            duration-150
            focus:shadow-outline
          `}
            >
              Check in
            </button>
            <button
              disabled={!isCheckout}
              onClick={(e) => {
                console.log("isCheckout", isCheckout);
                e.preventDefault();
                setIsCheckOut(!isCheckout);
              }}
              type="submit"
              className={`h-10
              px-5
              text-indigo-100
              ${isCheckout ? "bg-indigo-700" : "bg-slate-500"}
              rounded-lg
              transition-colors
              duration-150
              focus:shadow-outline
             `}
            >
              Check out
            </button>
          </div>
          <div></div>
        </form>
      </div>
    </div>
  );
};

export default Attendance;
