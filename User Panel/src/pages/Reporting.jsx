import React from "react";

const Reporting = () => {
  return (
    <div className="relative flex flex-col justify-center min-h-screen overflow-hidden ">
      <div className="w-full p-6 m-auto bg-white rounded-md shadow-xl shadow-rose-600/40 ring-2 ring-indigo-600 lg:max-w-xl">
        <h1 className="text-3xl font-semibold text-center text-indigo-700 ">
          Reporting
        </h1>
        <form className="mt-6">
          <div className="mb-2 flex items-stretch">
            <label>
              <span className="text-gray-700">Your name</span>
              <input
                type="text"
                name="name"
                className="

            w-64
            mr-4
            block px-16 py-2 mt-2
            border-gray-300
            rounded-md
            shadow-sm
            focus:border-indigo-300
            focus:ring
            focus:ring-indigo-200
            focus:ring-opacity-50
          "
                placeholder="Shubham Paranjape"
              />
            </label>
            <label>
              <span className="text-gray-700">Date</span>
              <input
                name="department"
                type="date"
                className="
            block
            w-64
            mt-2 px-16 py-2
            border-gray-300
            rounded-md
            shadow-sm
            focus:border-indigo-300
            focus:ring
            focus:ring-indigo-200
            focus:ring-opacity-50
          "
                required
              />
            </label>
          </div>
          {/* <div className='mb-2'>
            <label>
              <span className='text-gray-700'>Department</span>
              <input
                name='department'
                type='text'
                className='
            block
            w-full
            mt-2 px-16 py-2
            border-gray-300
            rounded-md
            shadow-sm
            focus:border-indigo-300
            focus:ring
            focus:ring-indigo-200
            focus:ring-opacity-50
          '
                placeholder='SDET'
                required
              />
            </label>
          </div> */}
          <div className="mb-2 flex items-stretch">
            <label>
              <span className="text-gray-700">In time</span>
              <input
                name="department"
                type="time"
                className="
            block
            w-64
            mr-4
            mt-2 px-16 py-2
            border-gray-300
            rounded-md
            shadow-sm
            focus:border-indigo-300
            focus:ring
            focus:ring-indigo-200
            focus:ring-opacity-50
          "
                required
              />
            </label>
            <label>
              <span className="text-gray-700">Out time</span>
              <input
                name="department"
                type="time"
                className="
            block
            w-64
            mt-2 px-16 py-2
            border-gray-300
            rounded-md
            shadow-sm
            focus:border-indigo-300
            focus:ring
            focus:ring-indigo-200
            focus:ring-opacity-50
          "
                required
              />
            </label>
          </div>

          <div className="mb-2">
            <label>
              <span class="text-gray-700">Reason for Reporting</span>
              <textarea
                name="message"
                className="
            block
            w-full
            mt-2 px-16 py-8
            border-gray-300
            rounded-md
            shadow-sm
            focus:border-indigo-300
            focus:ring
            focus:ring-indigo-200
            focus:ring-opacity-50
          "
                rows="4"
              ></textarea>
            </label>
          </div>

          <div class="mb-6">
            <button
              type="submit"
              className="
            h-10
            px-5
            text-indigo-100
            bg-indigo-700
            rounded-lg
            transition-colors
            duration-150
            focus:shadow-outline
            hover:bg-indigo-800
          "
            >
              Submit
            </button>
          </div>
          <div></div>
        </form>
      </div>
    </div>
  );
};

export default Reporting;
